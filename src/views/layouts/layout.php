<?php declare(strict_types=1);

use yii\BaseYii;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\db\ActiveRecord;
use yii\web\View;
use Yii2Extended\Metadata\BundleInterface;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\RecordInterface;

/** @var View $this */
/** @var null|array<integer, ModuleInterface> $modules */
/** @var ?ModuleInterface $module */
/** @var ?BundleInterface $bundle */
/** @var ?RecordInterface $record */
/** @var ?ActiveRecord $model */
/** @var string $content */
/** @author Anastaszor */
if(!isset($modules))
{
	$modules = [];
}
if(!isset($module))
{
	$module = null;
}
if(!isset($bundle))
{
	$bundle = null;
}
if(!isset($record))
{
	$record = null;
}
if(!isset($model))
{
	$model = null;
}

if(null !== $module)
{
	$this->title = BaseYii::t('CrudModule.View', 'Dashboard {module}', ['module' => $module->getLabel()]);
	$this->params['breadcrumbs'][] = [
		'label' => BaseYii::t('CrudModule.View', 'CRUD'),
		'url' => ['dashboard/index'],
	];
	$this->params['breadcrumbs'][] = [
		'label' => $module->getLabel(),
		'url' => ['dashboard/module', 'moduleId' => $module->getId()],
	];
	
	if(null !== $bundle)
	{
		$this->params['breadcrumbs'][] = [
			'label' => $bundle->getLabel(),
			'url' => ['dashboard/module', 'moduleId' => $module->getId(), 'bundleId' => $bundle->getId()],
		];
		
		if(null !== $record)
		{
			$this->params['breadcrumbs'][] = [
				'label' => $record->getLabel(),
				'url' => ['crud/index', 'moduleId' => $module->getId(), 'bundleId' => $bundle->getId(), 'recordId' => $record->getId()],
			];
			
			if(null !== $model)
			{
				$this->params['breadcrumbs'][] = [
					'label' => \implode(', ', $model->getPrimaryKey(true)),
					'url' => ['crud/view', 'moduleId' => $module->getId(), 'bundleId' => $bundle->getId(), 'recordId' => $record->getId()] + (array) $model->getPrimaryKey(true),
				];
			}
		}
	}
}
else
{
	$this->title = BaseYii::t('CrudModule.View', 'Crud Dashboard');
	$this->params['breadcrumbs'][] = ['label' => $this->title];
}

$items = [];
$items[] = [
	'label' => '<i class="bi-grid"></i>'.Html::encode(BaseYii::t('CrudModule.View', 'General Dashboard')),
	'url' => ['dashboard/index'],
	'active' => null === $module,
	'linkOptions' => [
		'class' => 'link-dark',
	],
];

/** @var ModuleInterface $smodule */
foreach($modules as $smodule)
{
	if(null !== $module && $smodule->getId() === $module->getId())
	{
		/** @var BundleInterface $sbundle */
		foreach($smodule->getBundles() as $sbundle)
		{
			$submenu = [
				'label' => Html::encode($smodule->getLabel()).' &raquo; '.Html::encode($sbundle->getLabel()),
				'url' => ['crud/index', 'moduleId' => $smodule->getId(), 'bundleId' => $sbundle->getId()],
				'active' => false,
				'linkOptions' => [
					'class' => 'link-dark',
				],
			];
			
			/** @var RecordInterface $srecord */
			foreach($sbundle->getEnabledRecords() as $srecord)
			{
				$submenu['items'][] = [
					'label' => /* '<i class="bi-'.Html::encode($record->getBootstrapIconName()).'"></i>'. */ Html::encode($srecord->getLabel()),
					'url' => ['crud/index', 'moduleId' => $smodule->getId(), 'bundleId' => $sbundle->getId(), 'recordId' => $srecord->getId()],
					'active' => false,
					'linkOptions' => [
						'class' => 'link-dark',
					],
				];
			}
			
			$items[] = $submenu;
		}
	}
	else
	{
		$items[] = [
			'label' => '<i class="bi-'.Html::encode($smodule->getBootstrapIconName()).'"></i>'.Html::encode($smodule->getLabel()),
			'url' => ['dashboard/module', 'moduleId' => $smodule->getId()],
			'active' => null !== $module && $smodule->getId() === $module->getId(),
			'linkOptions' => [
				'class' => 'link-dark',
			],
		];
	}
}
?>
<div class="container-fluid">
<div class="row">
<div class="col-md-3 col-lg-2 d-flex flex-column flex-shrink-0 bg-light">
<div class="position-sticky pt-3 pb-3">
<?php echo Nav::widget([
	'items' => $items,
	'encodeLabels' => false,
	'activateParents' => true,
	'options' => [
		'class' => 'nav nav-pills flex-column mb-auto',
	],
]); ?>
</div>
</div>
<div class="col-md-9 col-lg-10 pt-3 pb-3">

<?php echo Breadcrumbs::widget([
	'links' => $this->params['breadcrumbs'] ?? [],
]); ?>

<?php if(null !== $module)
{ ?>
<ul class="nav nav-tabs nav-fill">
<?php foreach($module->getBundles() as $sbundle)
{ ?>
	<li class="nav-item">
		<?php echo Html::a(Html::encode($sbundle->getLabel()), ['dashboard/module', 'moduleId' => $module->getId(), 'bundleId' => $sbundle->getId()], [
			'class' => 'nav-link'.(null !== $bundle && $bundle->getId() === $sbundle->getId() ? ' active' : ''),
		]); ?>
	</li>
<?php } ?>
</ul>
<br>
<?php } ?>

<?php echo $content; ?>

</div>
</div>
</div>
