<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-crud-html library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use yii\BaseYii;
use yii\bootstrap5\Html;
use yii\bootstrap5\LinkPager;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecordInterface;
use yii\grid\ActionColumn;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\View;
use Yii2Extended\Metadata\BundleInterface;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\RecordInterface;

/** @var View $this */
/** @var array<integer, ModuleInterface> $modules */
/** @var ModuleInterface $module */
/** @var BundleInterface $bundle */
/** @var RecordInterface $record */
/** @var ?ActiveRecordInterface $model */
/** @var ActiveDataProvider $dataProvider */
/** @author Anastaszor */
$this->beginContent(__DIR__.'/../layouts/layout.php', [
	'modules' => $modules,
	'module' => $module,
	'bundle' => $bundle,
	'record' => $record,
	'model' => $model,
]); ?>

<p><?php echo Html::a(
	BaseYii::t('CrudModule.View', 'Create new {label}', ['label' => $record->getLabel()]),
	['crud/create', 'moduleId' => $module->getId(), 'bundleId' => $bundle->getId(), 'recordId' => $record->getId()],
	['class' => 'btn btn-success'],
); ?></p>

<?php $columns = [
	[
		'class' => SerialColumn::class,
	],
];

$count = 0;

foreach($record->getFieldNames() as $fieldName)
{
	if(\mb_strpos($fieldName, 'meta_') === 0 || \mb_strpos($fieldName, 'comment') !== false)
	{
		continue;
	}
	if(3 < $count)
	{
		break;
	}
	$count++;
	$columns[] = [
		'class' => DataColumn::class,
		'attribute' => $fieldName,
	];
}

$columns[] = [
	'class' => ActionColumn::class,
	'urlCreator' => /** @psalm-suppress MissingClosureParamType */ function(string $action, ActiveRecordInterface $model, $key, $index, ActionColumn $column) use ($module, $bundle, $record) : string
	{
		$key = ['moduleId' => $module->getId(), 'bundleId' => $bundle->getId(), 'recordId' => $record->getId()] + (array) $model->getPrimaryKey(true);
		
		$params = (array) $key;
		$params[0] = null !== $column->controller ? $column->controller.'/'.$action : $action;
		
		return Url::toRoute($params);
	},
];

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'columns' => $columns,
	'pager' => [
		'class' => LinkPager::class,
		'options' => [
			'class' => 'pagination',
		],
		'linkContainerOptions' => [
			'class' => 'page-item',
		],
		'linkOptions' => [
			'class' => 'page-link',
		],
		'prevPageLabel' => '&lsaquo;',
		'nextPageLabel' => '&rsaquo;',
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo',
		'registerLinkTags' => true,
		'disabledListItemSubTagOptions' => [
			'tag' => 'a',
			'class' => 'page-link',
		],
	],
	'formatter' => [
		'class' => Formatter::class,
		'nullDisplay' => '<span class="text-muted">'.Html::encode(BaseYii::t('yii', '(not set)')).'</span>',
	],
]); ?>

<?php $this->endContent();
