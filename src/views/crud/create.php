<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-crud-html library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use yii\BaseYii;
use yii\db\ActiveRecordInterface;
use yii\helpers\Html;
use yii\web\View;
use Yii2Extended\Metadata\BundleInterface;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\RecordInterface;

/** @var View $this */
/** @var array<integer, ModuleInterface> $modules */
/** @var ModuleInterface $module */
/** @var BundleInterface $bundle */
/** @var RecordInterface $record */
/** @var ActiveRecordInterface $model */
/** @author Anastaszor */
$this->beginContent(__DIR__.'/../layouts/layout.php', [
	'modules' => $modules,
	'module' => $module,
	'bundle' => $bundle,
	'record' => $record,
	'model' => $model,
]);

$this->title = BaseYii::t('CrudModule.View', 'Create new {label}', ['label' => $record->getLabel()]);
$this->params['breadcrumbs'][] = ['label' => BaseYii::t('CrudModule.View', 'Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<h1 class="col-10 offset-1"><?php 
/** @psalm-suppress MixedArgumentTypeCoercion */
echo Html::encode(((string) $record->getLabel()).' '.\implode(', ', (array) $model->getPrimaryKey(true)));
?></h1>

<?php echo (string) $this->render('_form', [
	'model' => $model,
]); ?>
	
<?php $this->endContent();
