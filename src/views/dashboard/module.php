<?php declare(strict_types=1);

use yii\BaseYii;
use yii\bootstrap5\ButtonGroup;
use yii\bootstrap5\Html;
use yii\helpers\Url;
use yii\web\View;
use Yii2Extended\Metadata\BundleInterface;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\RecordInterface;

/** @var View $this */
/** @var array<integer, ModuleInterface> $modules */
/** @var ModuleInterface $module */
/** @var ?BundleInterface $bundle */
/** @var ?RecordInterface $record */
/** @var ?ActiveRecordInterface $model */
/** @author Anastaszor */
$this->beginContent(__DIR__.'/../layouts/layout.php', [
	'modules' => $modules,
	'module' => $module,
	'bundle' => $bundle,
	'record' => $record,
	'model' => $model,
]); ?>

<table class="table table-striped table-hover">
	<tr>
		<th scope="col"><?php echo Html::encode(BaseYii::t('CrudModule.View', 'Record Class')); ?></th>
		<th scope="col"><?php echo Html::encode(BaseYii::t('CrudModule.View', 'Number of Records')); ?></th>
		<th scope="col"><?php echo Html::encode(BaseYii::t('CrudModule.View', 'Actions')); ?></th>
	</tr>
<?php foreach($module->getBundles() as $sbundle)
{ ?>
<?php if(null !== $bundle && $sbundle->getId() === $bundle->getId())
{ ?>
<?php /** @var RecordInterface $recordMd */ ?>
<?php foreach($sbundle->getEnabledRecords() as $recordMd)
{ ?>
	<tr>
		<td><i class="bi-<?php echo Html::encode($recordMd->getBootstrapIconName()); ?>"></i><?php echo Html::encode($recordMd->getClass()); ?></td>
		<td><?php echo Html::encode((string) $recordMd->getClass()::find()->count()); ?></td>
		<td>
<?php echo ButtonGroup::widget([
	'buttons' => [
		[
			'label' => '<i class="bi-eye"></i>'.Html::encode(BaseYii::t('CrudModule.View', 'View List')),
			'tagName' => 'a',
			'options' => [
				'href' => Url::to(['crud/index', 'moduleId' => $module->getId(), 'bundleId' => $sbundle->getId(), 'recordId' => $recordMd->getId()]),
				'class' => 'btn-primary'.($sbundle->getId() === $bundle->getId() ? ' active' : ''),
			],
		],
	],
	'encodeLabels' => false,
]); ?>
		</td>
	</tr>
<?php } ?>
<?php } ?>
<?php } ?>
</table>

<?php $this->endContent();
