<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-crud library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Crud\Controllers;

use RuntimeException;
use yii\BaseYii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii2Module\Yii2Crud\Components\RbacAccessRule;
use Yii2Module\Yii2Crud\CrudModule;

/**
 * DashboardController class file.
 * 
 * This controller shows all the available modules to crud generates links for
 * them to be available to manage.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.StaticAccess")
 */
class DashboardController extends Controller
{
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Component::behaviors()
	 * @return array<string, array<string, string|array<int|string, array<int|string, bool|string|array<int, string>>>>>
	 */
	public function behaviors() : array
	{
		return [
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'index' => ['GET'],
				],
			],
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'class' => RbacAccessRule::class,
						'actions' => ['index', 'module'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
	
	/**
	 * Displays the dashboard with all the registered modules.
	 * 
	 * @return string|Response
	 * @throws \yii\base\InvalidArgumentException
	 * @throws RuntimeException
	 */
	public function actionIndex()
	{
		/** @var CrudModule $crudModule */
		$crudModule = CrudModule::getInstance();
		$modules = $crudModule->getVerifiedModules();
		
		return $this->render('dashboard', [
			'modules' => $modules,
			'module' => null,
			'bundle' => null,
			'record' => null,
			'model' => null,
		]);
	}
	
	/**
	 * @param string $moduleId
	 * @param ?string $bundleId
	 * @return string|Response
	 * @throws \yii\base\InvalidArgumentException
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	public function actionModule(string $moduleId, ?string $bundleId = null)
	{
		/** @var CrudModule $crudModule */
		$crudModule = CrudModule::getInstance();
		$modules = $crudModule->getVerifiedModules();
		
		/** @var ?\Yii2Extended\Metadata\ModuleInterface $smodule */
		$smodule = null;
		
		foreach($modules as $module)
		{
			if($module->id === $moduleId)
			{
				$smodule = $module;
				
				break;
			}
		}
		
		if(null === $smodule)
		{
			$message = 'Failed to find module {id}';
			$context = ['id' => $moduleId];
			
			throw new NotFoundHttpException(BaseYii::t('CrudModule.View', $message, $context));
		}
		
		/** @var ?\Yii2Extended\Metadata\BundleInterface $sbundle */
		$sbundle = null;
		
		foreach($smodule->getBundles() as $bundle)
		{
			if(null === $bundleId || $bundle->getId() === $bundleId)
			{
				$sbundle = $bundle;
				
				break;
			}
		}
		
		if(null === $sbundle)
		{
			$message = 'Failed to find bundle {bid} in module {mid}';
			$context = ['bid' => $bundleId, 'mid' => $moduleId];
			
			throw new NotFoundHttpException(BaseYii::t('CrudModule.View', $message, $context));
		}
		
		return $this->render('module', [
			'modules' => CrudModule::getInstance()->getVerifiedModules(),
			'module' => $smodule,
			'bundle' => $sbundle,
			'record' => null,
			'model' => null,
		]);
	}
	
}
