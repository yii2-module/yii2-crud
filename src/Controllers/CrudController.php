<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-crud library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Crud\Controllers;

use Exception;
use InvalidArgumentException;
use LogicException;
use RuntimeException;
use Throwable;
use yii\BaseYii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use Yii2Extended\Metadata\BundleInterface;
use Yii2Extended\Metadata\ModuleInterface;
use Yii2Extended\Metadata\RecordInterface;
use Yii2Module\Yii2Crud\Components\RbacAccessRule;
use Yii2Module\Yii2Crud\CrudModule;

/**
 * CrudController class file.
 * 
 * This controller performs CRUD operations on a given model class.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.StaticAccess")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class CrudController extends Controller
{
	
	/**
	 * {@inheritDoc}
	 * @see \yii\base\Component::behaviors()
	 * @return array<string, array<string, string|array<integer|string, array<integer|string, boolean|string|array<integer, string>>>>>
	 */
	public function behaviors() : array
	{
		return [
			'verbs' => [
				'class' => VerbFilter::class,
				'actions' => [
					'index' => ['GET'],
					'create' => ['GET', 'POST'],
					'view' => ['GET'],
					'update' => ['GET', 'POST'],
					'delete' => ['POST'],
					'search' => ['GET'],
				],
			],
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'class' => RbacAccessRule::class,
						'actions' => ['index', 'create', 'view', 'update', 'delete', 'search'],
						'allow' => true,
						'roles' => ['@'],
						'permTemplate' => 'crud|class|{action}|{module}|{bundle}|{record}',
					],
				],
			],
		];
	}
	
	/**
	 * Lists all Log models.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @param string $recordId
	 * @param Request $request
	 * @return string|Response
	 * @throws ForbiddenHttpException
	 * @throws InvalidArgumentException if the view file or the layout file does not exist
	 * @throws LogicException if the record class does not exist
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	public function actionIndex(string $moduleId, string $bundleId, string $recordId, Request $request)
	{
		$modelClass = $this->getModelClass($moduleId, $bundleId, $recordId, RecordInterface::ACTION_INDEX);
		/** @psalm-suppress UnsafeInstantiation */
		$model = new $modelClass();
		$model->setAttributes($request->queryParams);
		
		/** @var ActiveQuery $query */
		$query = $modelClass::find();
		$count = 1;
		
		foreach($model->getAttributes() as $attrName => $attrValue)
		{
			if(null !== $attrValue)
			{
				$query->andWhere([
					'=',
					':name'.((string) $count),
					':value'.((string) $count),
				], [
					':name'.((string) $count) => $attrName,
					':value'.((string) $count) => $attrValue,
				]);
				$count++;
			}
		}
		
		return $this->render('index', [
			'modules' => CrudModule::getInstance()->getVerifiedModules(),
			'module' => $this->getModuleMetadata($moduleId),
			'bundle' => $this->getBundleMetadata($moduleId, $bundleId),
			'record' => $this->getRecordMetadata($moduleId, $bundleId, $recordId),
			'model' => null,
			'dataProvider' => new ActiveDataProvider([
				'query' => $query,
				'pagination' => [
					'class' => Pagination::class,
					'defaultPageSize' => 50,
				],
			]),
		]);
	}
	
	/**
	 * Displays a single record.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @param string $recordId
	 * @param Request $request
	 * @return string|Response
	 * @throws ForbiddenHttpException
	 * @throws InvalidArgumentException if the view file or the layout file does not exist
	 * @throws LogicException if the record class does not exist
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	public function actionView(string $moduleId, string $bundleId, string $recordId, Request $request)
	{
		$model = $this->findModel($moduleId, $bundleId, $recordId, RecordInterface::ACTION_VIEW, $request);
		
		return $this->render('view', [
			'modules' => CrudModule::getInstance()->getVerifiedModules(),
			'module' => $this->getModuleMetadata($moduleId),
			'bundle' => $this->getBundleMetadata($moduleId, $bundleId),
			'record' => $this->getRecordMetadata($moduleId, $bundleId, $recordId),
			'model' => $model,
		]);
	}
	
	/**
	 * Creates a new Log model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @param string $recordId
	 * @param Request $request
	 * @return string|Response
	 * @throws \yii\db\Exception
	 * @throws ForbiddenHttpException
	 * @throws InvalidArgumentException if the view file or the layout file does not exist
	 * @throws LogicException if the record class does not exist
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	public function actionCreate(string $moduleId, string $bundleId, string $recordId, Request $request)
	{
		$modelClass = $this->getModelClass($moduleId, $bundleId, $recordId, RecordInterface::ACTION_CREATE);
		/** @psalm-suppress UnsafeInstantiation */
		$model = new $modelClass();
		
		if($model->load((array) $request->post()) && $model->save())
		{
			return $this->redirect(\array_merge(['view', 'moduleId' => $moduleId, 'bundleId' => $bundleId, 'recordId' => $recordId], (array) $model->getPrimaryKey(true)));
		}
		
		return $this->render('create', [
			'modules' => CrudModule::getInstance()->getVerifiedModules(),
			'module' => $this->getModuleMetadata($moduleId),
			'bundle' => $this->getBundleMetadata($moduleId, $bundleId),
			'record' => $this->getRecordMetadata($moduleId, $bundleId, $recordId),
			'model' => $model,
		]);
	}
	
	/**
	 * Updates an existing Log model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @param string $recordId
	 * @param Request $request
	 * @return string|Response
	 * @throws \yii\db\Exception
	 * @throws ForbiddenHttpException
	 * @throws InvalidArgumentException if the view file or the layout file does not exist
	 * @throws LogicException if the record class does not exist
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	public function actionUpdate(string $moduleId, string $bundleId, string $recordId, Request $request)
	{
		$model = $this->findModel($moduleId, $bundleId, $recordId, RecordInterface::ACTION_UPDATE, $request);
		
		if($model->load((array) $request->post()) && $model->save())
		{
			return $this->redirect(\array_merge(['view'], (array) $model->getPrimaryKey(true)));
		}
		
		return $this->render('update', [
			'modules' => CrudModule::getInstance()->getVerifiedModules(),
			'module' => $this->getModuleMetadata($moduleId),
			'bundle' => $this->getBundleMetadata($moduleId, $bundleId),
			'record' => $this->getRecordMetadata($moduleId, $bundleId, $recordId),
			'model' => $model,
		]);
	}
	
	/**
	 * Deletes a given record.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @param string $recordId
	 * @param Request $request
	 * @return string|Response
	 * @throws \yii\db\StaleObjectException
	 * @throws Exception
	 * @throws ForbiddenHttpException
	 * @throws LogicException if the record class does not exist
	 * @throws NotFoundHttpException
	 * @throws Throwable
	 */
	public function actionDelete(string $moduleId, string $bundleId, string $recordId, Request $request)
	{
		$model = $this->findModel($moduleId, $bundleId, $recordId, RecordInterface::ACTION_DELETE, $request);
		
		$model->delete();
		
		return $this->redirect(['index', 'moduleId' => $moduleId, 'bundleId' => $bundleId, 'recordId' => $recordId]);
	}
	
	/**
	 * Gets the module that is also a bundle with the given module id.
	 * 
	 * @param string $moduleId
	 * @return \yii\base\Module&ModuleInterface
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	protected function getModuleMetadata(string $moduleId) : ModuleInterface
	{
		/** @var CrudModule $crudModule */
		$crudModule = CrudModule::getInstance();
		$modules = $crudModule->getVerifiedModules();
		
		foreach($modules as $module)
		{
			if($module->id === $moduleId)
			{
				return $module;
			}
		}
		
		$message = 'Failed to find module with id {id}';
		$context = ['id' => $moduleId];
		
		throw new NotFoundHttpException(BaseYii::t('CrudModule.View', $message, $context));
	}
	
	/**
	 * Gets the bundle metadata from the bundles mwith the given ids.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @return BundleInterface
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	protected function getBundleMetadata(string $moduleId, string $bundleId) : BundleInterface
	{
		/** @var ModuleInterface $module */
		$module = $this->getModuleMetadata($moduleId);
		
		/** @var BundleInterface $bundle */
		foreach($module->getBundles() as $bundle)
		{
			if($bundle->getId() === $bundleId)
			{
				return $bundle;
			}
		}
		
		$message = 'Failed to find bundle with id {bid} in module {mid}';
		$context = ['bid' => $bundleId, 'mid' => $moduleId];
		
		throw new NotFoundHttpException(BaseYii::t('CrudModule.View', $message, $context));
	}
	
	/**
	 * Gets the record metadata from the bundles module with the given record id.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @param string $recordId
	 * @return RecordInterface
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	protected function getRecordMetadata(string $moduleId, string $bundleId, string $recordId) : RecordInterface
	{
		$bundle = $this->getBundleMetadata($moduleId, $bundleId);
		
		/** @var RecordInterface $record */
		foreach($bundle->getEnabledRecords() as $record)
		{
			if($record->getId() === $recordId)
			{
				return $record;
			}
		}
		
		$message = 'Failed to find record class with id {rid} in bundle {bid} in module {mid}';
		$context = ['rid' => $recordId, 'bid' => $bundleId, 'mid' => $moduleId];
		
		throw new NotFoundHttpException(BaseYii::t('CrudModule.View', $message, $context));
	}
	
	/**
	 * Gets the class that correspond to the module and record.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @param string $recordId
	 * @param string $action
	 * @return class-string<ActiveRecord>
	 * @throws ForbiddenHttpException
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	protected function getModelClass(string $moduleId, string $bundleId, string $recordId, string $action) : string
	{
		$metadata = $this->getRecordMetadata($moduleId, $bundleId, $recordId);
		
		if(!$metadata->isAllowed($action))
		{
			$message = 'This action is not allowed';
			
			throw new ForbiddenHttpException(BaseYii::t('CrudModule.View', $message));
		}
		
		$class = $metadata->getClass();
		
		if(\is_subclass_of($class, ActiveRecord::class))
		{
			return $class;
		}
		
		$message = 'Failed to find real record class with id {id}';
		$context = ['id' => $recordId];
		
		throw new NotFoundHttpException(BaseYii::t('CrudModule.View', $message, $context));
	}
	
	/**
	 * Finds the model from the request params.
	 * 
	 * @param string $moduleId
	 * @param string $bundleId
	 * @param string $recordId
	 * @param string $action
	 * @param Request $request
	 * @return ActiveRecord
	 * @throws ForbiddenHttpException
	 * @throws LogicException if the record class does not exist
	 * @throws NotFoundHttpException
	 * @throws RuntimeException
	 */
	protected function findModel(string $moduleId, string $bundleId, string $recordId, string $action, Request $request) : ActiveRecord
	{
		$modelClass = $this->getModelClass($moduleId, $bundleId, $recordId, $action);
		
		$pks = [];
		
		foreach($modelClass::primaryKey() as $pkname)
		{
			if(isset($request->queryParams[$pkname]))
			{
				$pks[$pkname] = $request->queryParams[$pkname];
				
				continue;
			}
			
			$message = BaseYii::t('CrudModule.Module', 'Failed to find key {name} in request param');
			$context = ['{name}' => $pkname];
			
			throw new NotFoundHttpException(\strtr($message, $context));
		}
		
		$model = $modelClass::findOne($pks);
		if(null !== $model)
		{
			return $model;
		}
		
		$message = BaseYii::t('CrudModule.Module', 'The requested page does not exist.');
		
		throw new NotFoundHttpException($message);
	}
	
}
