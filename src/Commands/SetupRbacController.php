<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-crud library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Crud\Commands;

use InvalidArgumentException;
use yii\console\ExitCode;
use Yii2Module\Helper\Commands\ExtendedController;
use Yii2Module\Yii2Crud\Components\RbacStructure;
use Yii2Module\Yii2Crud\CrudModule;

/**
 * This class sets up the rbac roles and permissions that are to be used in this
 * module to show users their permissions. This setup is idempotent and does
 * not destroy existing relationships, but repairs itself in case of errors.
 * 
 * @author Anastaszor
 */
class SetupRbacController extends ExtendedController
{
	
	/**
	 * Effectively repairs the rbac structure of this module.
	 * 
	 * @param string $moduleId
	 * @return integer
	 */
	public function actionModule(string $moduleId) : int
	{
		return $this->runCallable(function() use ($moduleId) : int
		{
			$crudModule = CrudModule::getInstance();
			$targetModule = $crudModule->getBundle($moduleId);
			if(null === $targetModule)
			{
				$message = 'Failed to find module with bundle for id {id}, check warning logs';
				$context = ['id' => $moduleId];
				
				throw new InvalidArgumentException(\strtr($message, $context));
			}
			
			$rbacStructure = new RbacStructure($this->getLogger());
			$rbacStructure->createAllPermissions($targetModule);

			return ExitCode::OK;
		});
	}
	
}
